package com.opetryshyn;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Teacher {

    private String surname;

    private String course;

    private List<String> studentList;

    public Teacher(String surname, String course, List<String> studentList) {
        this.surname = surname;
        this.course = course;
        this.studentList = studentList;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public List<String> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<String> studentList) {
        this.studentList = studentList;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "surname='" + surname + '\'' +
                ", course='" + course + '\'' +
                ", studentList=" + studentList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return surname.equals(teacher.surname) && course.equals(teacher.course) && studentList.equals(teacher.studentList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surname, course, studentList);
    }

    public static Teacher fromString(String data){
        String[] tokens = data.split("\t");
        return new Teacher(tokens[0], tokens[1], Arrays.stream(tokens[2].split(" ")).toList());
    }
}
