package com.opetryshyn;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static final String FILEPATH_1 = "file1.txt";
    private static final String FILEPATH_2 = "file2.txt";

    private static final Scanner scanner = new Scanner(System.in);
    private static final Map<String, List<String>> studentsCourses = new HashMap<>();

    private static List<String> readLinesFromFile(String filepath) throws NoSuchFileException {
        List<String> lines;
        try (Stream<String> lineStream = Files.lines(Paths.get(filepath))) {
            lines = lineStream.collect(Collectors.toList());
        } catch (IOException e) {
            throw new NoSuchFileException("Invalid file path");
        }
        return lines;
    }


    private static List<Teacher> readNewspapersFromFile(String filepath) throws NoSuchFileException {
        return readLinesFromFile(filepath)
                .stream()
                .map(Teacher::fromString)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) throws NoSuchFileException {

        List<Teacher> teachers = readNewspapersFromFile(FILEPATH_1);
        List<Teacher> teachers2 = readNewspapersFromFile(FILEPATH_2);
        initStudentsCoursesMap(teachers);

        //  List<Teacher> otherTeachers = readNewspapersFromFile(FILEPATH_2);

        printMenu();

        while (true) {
            switch (scanner.nextInt()) {
                case 1 -> task1();
                case 2 -> task2(teachers);
                case 3 -> task3(teachers, teachers2);
                case 4 -> {
                    System.out.println("Enter the names of two courses:");
                    String course1 = scanner.next();
                    String course2 = scanner.next();
                    task2Additional(teachers, course1, course2);
                }
                default -> {
                    return;
                }
            }
        }

    }

    private static void task2Additional(List<Teacher> teachers, String course1, String course2) {
        Set<String> courses = teachers.stream().map(Teacher::getCourse).collect(Collectors.toSet());
        if (!(courses.contains(course1) && courses.contains(course2))) {
            System.out.println("One ot both of the courses you wrote are not present");
            return;
        }
        Map<String, List<String>> coursesInfo = new HashMap<>();
        teachers.forEach(x -> {
            coursesInfo.computeIfAbsent(x.getCourse(), k -> new ArrayList<>());
            coursesInfo.get(x.getCourse()).addAll(x.getStudentList());
        });
        List<String> firstCourseAttenders = coursesInfo.get(course1);
        List<String> secondCourseAttenders = coursesInfo.get(course2);

        firstCourseAttenders.stream()
                .distinct()
                .filter(secondCourseAttenders::contains)
                .forEach(System.out::println);

    }

    private static void printMenu() {
        System.out.println("1 - task 1");
        System.out.println("2 - task 2");
        System.out.println("3 - task 3");
        System.out.println("4 - task 2 additional");

    }

    private static void task1() {
        studentsCourses.forEach((key, value) -> System.out.println(key + ":" + value));
    }

    private static void initStudentsCoursesMap(List<Teacher> teachers) {
        teachers.forEach(x -> x.getStudentList().forEach(s -> {
            studentsCourses.computeIfAbsent(s, k -> new ArrayList<>());
            studentsCourses.get(s).add(x.getCourse());
        }));
    }

    private static void task2(List<Teacher> teachers) {
        Set<String> courses = teachers.stream().map(Teacher::getCourse).collect(Collectors.toSet());
        studentsCourses.entrySet().stream().filter(x -> x.getValue().size() == courses.size()).map(Map.Entry::getKey).forEach(System.out::println);
    }

    private static void task3(List<Teacher> teachers, List<Teacher> teachers2) {

        List<Teacher> allTeachers = new ArrayList<>();
        allTeachers.addAll(teachers);
        allTeachers.addAll(teachers2);

        Map<String, List<String>> result = new HashMap<>();

        allTeachers.forEach(x -> {
            result.computeIfAbsent(x.getSurname(), k -> new ArrayList<>());
            result.get(x.getSurname()).add(x.getCourse());
        });

        double avg = result.values().stream().mapToDouble(List::size).average().orElse(Double.NaN);
        result.entrySet().stream().filter(x -> x.getValue().size() > avg).map(Map.Entry::getKey).forEach(System.out::println);
    }
}
